module github.com/crewjam/saml

require (
	github.com/beevik/etree v1.0.1
	github.com/dchest/uniuri v0.0.0-20160212164326-8902c56451e9
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/jonboulle/clockwork v0.1.0
	github.com/kr/pretty v0.1.0
	github.com/kr/text v0.1.0
	github.com/russellhaering/goxmldsig v0.0.0-20180430223755-7acd5e4a6ef7
	github.com/zenazn/goji v0.0.0-20160507202103-64eb34159fe5
	golang.org/x/crypto v0.0.0-20180723164146-c126467f60eb
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127
)
